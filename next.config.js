const withTypescript = require("@zeit/next-typescript");

const config = require("./config");

const base = withTypescript();
base.serverRuntimeConfig = config;

module.exports = base;
