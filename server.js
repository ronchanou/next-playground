const { createServer } = require("http");
const { parse } = require("url");
const next = require("next");

const { serverRuntimeConfig } = require("./next.config");
const { createServerUnsplash } = require("./unsplash");

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const unsplash = createServerUnsplash(serverRuntimeConfig);

app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = parse(req.url, true);
    const { pathname, query } = parsedUrl;
    const pathTokens = pathname.split("/");
    const first = pathTokens[1];

    switch (first) {
      case "user":
        app.render(req, res, "/user", query);
        break;

      case "unsplash":
        res.setHeader("Content-Type", "application/json");
        const path = JSON.parse(query.path);
        const args = JSON.parse(query.args);
        try {
          unsplash({ path, args })
            .then(res => res.json())
            .then(json => {
              console.log(json);
              res.end(JSON.stringify(json));
            });
        } catch (ex) {
          console.error(ex);
          res.end(JSON.stringify(ex));
        }
        break;

      default:
        handle(req, res, parsedUrl);
    }
  }).listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
