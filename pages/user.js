import React from "react";
import { inject, observer } from "mobx-react";
import Link from "next/link";

import { createPageComponent } from "../components/page";

const findUsernameFromPage = page => page.query.username || page.asPath.split("/")[2];

const UserPage = ({ username, photos }) => (
  <div>
    {username}
    {photos && photos.map(p => <img key={p.id} src={p.urls.thumb || ""} />)}
    <Link href="/">
      <a>All</a>
    </Link>
  </div>
);

const ObservedUserPage = inject(context => {
  const { store, page } = context;
  const username = findUsernameFromPage(page);
  const photos = store.photos.filter(p => p.user.username === username);
  return { username, photos };
})(observer(UserPage));

const load = (store, page) => {
  const username = findUsernameFromPage(page);
  return store.requestUserPhotos(username);
};

export default createPageComponent(ObservedUserPage, load);
