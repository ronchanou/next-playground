import React from "react";
import { inject, observer } from "mobx-react";
import Link from "next/link";

import { createPageComponent } from "../components/page";

const PhotoPage = props => (
  <div>
    <div onClick={props.increment}>{props.counter}</div>

    {props.photos.map(p => (
      <Link key={p.id} href={`/user?username=${p.user.username}`}>
        <a>
          <img src={p.urls.thumb || ""} />
        </a>
      </Link>
    ))}
  </div>
);

const ObservedPhotoPage = inject(context => {
  const { photos } = context.store;
  return { photos };
})(observer(PhotoPage));

const load = store => store.requestPhotos();

export default createPageComponent(ObservedPhotoPage, load);
