import { types, flow, getEnv, getRoot } from "mobx-state-tree";

const optString = types.maybeNull(types.string);

export const Links = types.model({
  download: optString,
  downloadLocation: optString,
  html: optString,
  self: optString
});

export const ImageSet = types.model({
  large: optString,
  medium: optString,
  small: optString
});

export const User = types
  .model({
    id: types.string,
    username: types.identifier,
    firstName: optString,
    bio: optString,
    instagramUsername: optString,
    lastName: optString,
    links: Links,
    location: optString,
    name: optString,
    portfolioUrl: optString,
    profileImage: ImageSet,
    totalCollections: 0,
    totalLikes: 0,
    totalPhotos: 0,
    twitterUsername: optString,
    updatedAt: optString
  })
  .actions(self => ({
    loadPhotos: flow(function*() {
      const { unsplash } = getEnv(self);
      const response = yield unsplash({ path: ["users", "photos"], args: [self.username] });
      const rawPhotos = yield response.json();
      const root: any = getRoot(self);
      root.processRawPhotos(rawPhotos);
    })
  }));

export const Photo = types.model({
  id: types.identifier,
  categories: types.array(types.string),
  color: optString,
  createdAt: optString,
  currentUserCollections: types.array(types.string),
  description: optString,
  width: 0,
  height: 0,
  likedByUser: false,
  likes: 0,
  links: Links,
  slug: optString,
  sponsored: false,
  updatedAt: optString,
  urls: types.model({
    full: optString,
    raw: optString,
    regular: optString,
    small: optString,
    thumb: optString
  }),
  user: types.reference(User)
});

export const PhotoStore = types
  .model({
    photos: types.array(Photo),
    users: types.array(User)
  })
  .views(self => ({
    get photoMap() {
      const map = {};
      for (const photo of self.photos) {
        map[photo.id] = photo;
      }
      return map;
    },
    get userMap() {
      const map = {};
      for (const user of self.users) {
        map[user.username] = user;
      }
      return map;
    }
  }))
  .actions(self => ({
    processRawPhotos(rawPhotos: any[]) {
      for (const rp of rawPhotos) {
        const { user, ...rest } = rp;
        if (!self.userMap[user.username]) {
          self.users.push({
            ...user,
            instagramUsername: user.instagram_username,
            last_name: user.lastName,
            first_name: user.firstName,
            portfolioUrl: user.portfolio_url,
            profileImage: user.profile_image,
            totalCollections: user.total_collections,
            totalLikes: user.total_likes,
            totalPhotos: user.total_photos
          });
        }
        if (!self.photoMap[rp.id]) {
          self.photos.push({
            ...rest,
            user: user.username,
            currentUserCollections: rp.current_user_collections,
            likedByUser: rp.liked_by_user
          });
        }
      }
    }
  }))
  .actions(self => ({
    processPhotosResponse: flow(function*(response: any) {
      const rawPhotos: any[] = yield response.json();
      return self.processRawPhotos(rawPhotos);
    })
  }))
  .actions(self => ({
    requestPhotos: flow(function*() {
      const { unsplash } = getEnv(self);
      const response = yield unsplash({ path: ["photos", "listPhotos"], args: [2, 15, "latest"] });
      return self.processPhotosResponse(response);
    }),
    requestUserPhotos: flow(function*(username: string) {
      const { unsplash } = getEnv(self);
      const response = yield unsplash({ path: ["users", "photos"], args: [username] });
      const rawPhotos = yield response.json();
      return self.processRawPhotos(rawPhotos);
    })
  }));
