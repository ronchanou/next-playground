const Unsplash = require("unsplash-js").default;
require("isomorphic-unfetch");

const createServerUnsplash = config => {
  const unsplash = new Unsplash({
    applicationId: config.unsplashAccessKey,
    secret: config.unsplashSecretKey
  });

  return ({ path, args }) => {
    let func = unsplash;

    for (const key of path) {
      func = func[key];
    }

    return func(...args);
  };
};

const createClientUnsplash = () => {
  return ({ path, args = [] }) => {
    return fetch(`/unsplash?path=${JSON.stringify(path)}&args=${JSON.stringify(args)}`);
  };
};

module.exports = { createServerUnsplash, createClientUnsplash };
