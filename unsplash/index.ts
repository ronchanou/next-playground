/// <reference path="unsplash.d.ts"/>

import Unsplash from "unsplash-js";

declare module "unsplash-js";

interface ServerConfig {
  unsplashAccessKey: string;
  unsplashSecretKey: string;
}

interface UnsplashRequestInit {
  path: string[];
  args: any[];
}

export const createServerUnsplash = (config: ServerConfig) => {
  const unsplash = new Unsplash({
    applicationId: config.unsplashAccessKey
  });

  return ({ path, args }: UnsplashRequestInit) => {
    let func = unsplash;
    for (const key of path) {
      func = func[key];
    }
    return func(...args);
  };
};

export const createClientUnsplash = () => {
  return ({ path, args = [] }: UnsplashRequestInit) => {
    return fetch(`/unsplash?path=${JSON.stringify(path)}&args=${JSON.stringify(args)}`);
  };
};
