import { Provider } from "mobx-react";
import { isObservable } from "mobx";
import { connectReduxDevtools } from "mst-middlewares";
import Unsplash from "unsplash-js";
import "isomorphic-unfetch";

import { PhotoStore } from "../stores/photos";
import { createServerUnsplash, createClientUnsplash } from "../unsplash";
import getConfig from "next/config";

const { serverRuntimeConfig } = getConfig();

let storeSingleton;

const unsplash =
  typeof window === "undefined"
    ? createServerUnsplash(serverRuntimeConfig)
    : createClientUnsplash();

export const createPageComponent = (Component, load = () => {}) =>
  class Page extends React.Component {
    static async getInitialProps(page) {
      const { asPath, pathname, query } = page;

      const { req } = page;

      if (!req) {
        // if no req, this is running client-side, so bail out
        return { page: { asPath, pathname, query } };
      }

      // server-side only

      const store = PhotoStore.create({}, { unsplash });

      await load(store, page);

      return { store, page: { asPath, pathname, query } };
    }

    constructor(props) {
      super(props);

      // storeSingleton is only persisted and used by the client
      // use this fact to prevent duplicate data request on client (explained below)

      if (!storeSingleton) {
        // initial client render already has necessary data from server render
        // so no need to load data again
        // just initialize the global singleton which persists between page changes
        storeSingleton = PhotoStore.create(props.store, {
          location: typeof window !== "undefined" ? window.location : {},
          unsplash
        });
        if (process.env.NODE_ENV === "development") {
          connectReduxDevtools(require("remotedev"), storeSingleton);
        }
        return;
      }

      // if we made it here, the singleton was already initialized, so we're past the initial render
      // and coming from another page, so reload the data as if it were a separate server-rendered page
      load(storeSingleton, props.page);
    }

    render() {
      return (
        <Provider store={storeSingleton} page={this.props.page}>
          <Component />
        </Provider>
      );
    }
  };
